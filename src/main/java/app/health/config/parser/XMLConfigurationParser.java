package app.health.config.parser;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import app.health.config.HealthConfigurations;

public class XMLConfigurationParser implements ConfigurationParser {

	private HealthConfigurations healthConfigurations;

	public HealthConfigurations parse() {

		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(HealthConfigurations.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			//String fileName = "configuration.xml";
			ClassLoader classLoader = getClass().getClassLoader();
			//File file = new File(classLoader.getResource(fileName).getFile());
			//System.out.println("new config file exits?" + file.exists());
			
			/*if(file.exists())
			{
				System.out.println("Not able to read file");
				healthConfigurations = (HealthConfigurations) unmarshaller.unmarshal(file);
				
			}
			else
			{*/
				healthConfigurations = (HealthConfigurations) unmarshaller.unmarshal(classLoader.getResourceAsStream("configuration.xml"));
				
	//		}
			
			
			
			
		} catch (JAXBException e) {

			e.printStackTrace();
		}
		return healthConfigurations;

	}

}
