package app.health.config.parser;

import app.health.config.HealthConfigurations;

public interface ConfigurationParser {
	
	public HealthConfigurations parse();

}
