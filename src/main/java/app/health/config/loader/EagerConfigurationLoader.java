package app.health.config.loader;


import app.health.config.HealthConfigurations;
import app.health.config.parser.ConfigurationParser;
import app.health.config.parser.XMLConfigurationParser;

/**
 * Loads the configuration lazily.
 * Does not support dynamic modification of configuration.
 * @author Piyush Utreja
 *
 */
public class EagerConfigurationLoader {

	private ConfigurationParser parser;
	private final HealthConfigurations healthConfigurations;

	private static final EagerConfigurationLoader instance = new EagerConfigurationLoader();

	private EagerConfigurationLoader() {
		this.parser = new XMLConfigurationParser();
		this.healthConfigurations = this.parser.parse();
			}

	public static EagerConfigurationLoader loadConfiguration() {

		
		return instance;
	}

	public HealthConfigurations getConfiguration() {

		return healthConfigurations;

	}

}
