package app.health.config.loader;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Creates a new watcher thread which will detect changes in the configuration.
 * @author Piyush Utreja
 *
 */
public class ConfigurationWatcher extends Thread {

	private WatchHelper myWatch;
	static AtomicBoolean changed;

	public ConfigurationWatcher(AtomicBoolean changed) {
		ConfigurationWatcher.changed = changed;
		

	}

	@Override
	public void run() {
		myWatch = new WatchHelper();
		
		try {
			myWatch.watchRNDir("C:/Users/putrej/workspace/RestCrud/src/main/resources",changed);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
