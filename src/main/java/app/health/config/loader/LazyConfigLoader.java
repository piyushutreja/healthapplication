package app.health.config.loader;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import app.health.config.HealthConfigurations;
import app.health.config.parser.ConfigurationParser;
import app.health.config.parser.XMLConfigurationParser;

/**
 * Configuration Loader to load configuration lazily.
 * Supports dynamic modification of configuration.
 * Changes will be seen on next request.
 * @author Piyush Utreja
 *
 */
public class LazyConfigLoader {
	private ConfigurationParser parser;
	private static LazyConfigLoader instance;
	private final HealthConfigurations healthConfigurations;
	static ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	static Lock readLock = lock.readLock();
	static Lock writeLock = lock.writeLock();

	private LazyConfigLoader() {

		this.parser = new XMLConfigurationParser();
		this.healthConfigurations = this.parser.parse();

	}

	public static LazyConfigLoader loadConfiguration(AtomicBoolean changed) {
		writeLock.lock();
		try {
		if (instance == null || changed.compareAndSet(true, false)) {
			instance = new LazyConfigLoader();
		}
		return instance;
		} finally {
			writeLock.unlock();
		}
	}

	public  HealthConfigurations getConfiguration() {
		readLock.lock();
		try {
			return healthConfigurations;
		} finally {
			readLock.unlock();
		}

	}

}
