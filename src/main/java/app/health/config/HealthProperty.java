package app.health.config;

import javax.xml.bind.annotation.XmlAttribute;

public class HealthProperty {

	private String name;
	private String value;

	@XmlAttribute(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlAttribute(name = "value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
