package app.health.config;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@XmlRootElement(name="healthchecks")
public class HealthConfigurations {
	
	
	private AtomicBoolean modified= new AtomicBoolean();
	
	@XmlTransient
	public AtomicBoolean getModified() {
		return modified;
	}

	public void setModified(AtomicBoolean modified) {
		this.modified = modified;
	}

	

	private List<HealthConfiguration> configList;
	
	@XmlElement(name="healthcheck")
	public List<HealthConfiguration> getConfigList() {
		return configList;
	}

	public void setConfigList(List<HealthConfiguration> configList) {
		this.configList = configList;
	}


}
