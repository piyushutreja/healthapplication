package app.health.config;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class HealthConfiguration {

	private String type;
	private String id;
	private String visibility;

	@XmlAttribute(name = "visibility")
	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	private List<HealthProperty> propertyList;

	@XmlElement(name = "property")
	public List<HealthProperty> getPropertyList() {
		return propertyList;
	}

	public void setPropertyList(List<HealthProperty> propertyList) {
		this.propertyList = propertyList;
	}

	@XmlAttribute(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlAttribute(name = "id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
