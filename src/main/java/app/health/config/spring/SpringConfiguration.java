package app.health.config.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;


public class SpringConfiguration implements ApplicationContextAware {

	private static ApplicationContext context;

	public void setApplicationContext(ApplicationContext context) throws BeansException {

		this.context = context;
	}

	public static ApplicationContext getApplicationContext() {
		return context;
	}

}
