package app.health.Health;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import app.health.config.HealthConfigurations;
import app.health.config.loader.ConfigurationWatcher;
import app.health.manager.HealthManager;

public class HealthServlet  extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4722021700845745657L;
	String response ;
	HealthConfigurations healthConfigurations;
	static AtomicBoolean changed; 
	ConfigurationWatcher watcher ;
	

    public void init() throws ServletException {
    	changed = new AtomicBoolean();
    	watcher = new ConfigurationWatcher(changed);    	
    	watcher.start();
     	System.out.println("Intialized watch servcie in seperate thread");

    }    
	

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	
		HealthManager healthManager = new HealthManager();
	    response = healthManager.getResponse(req, resp,changed);		
		PrintWriter out = resp.getWriter();			
		out.write(response);	

	  

	}
}
