package app.health.marshaller;

public interface ResponseMarshallar  {
	
	public String marshall(Object obj);

}
