package app.health.marshaller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonMarshaller implements ResponseMarshallar {

	private ObjectMapper mapper;
	private String jsonInString;

	public String marshall(Object obj) {
		mapper = new ObjectMapper();

		try {

			jsonInString = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}

}
