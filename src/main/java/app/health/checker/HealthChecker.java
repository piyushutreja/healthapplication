package app.health.checker;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.health.config.HealthConfiguration;
import app.health.config.HealthConfigurations;
import app.health.entities.HealthInfo;
import app.health.handler.Handler;
import app.health.handler.map.HandlerMap;
/**
 *Iterates over each configuration configured in XML and calls respective
 *handler methods
 * @author Piyush Utreja
 *
 *
 */
public class HealthChecker {

	List<HealthInfo> health;

	public List<HealthInfo> checkHealth(HttpServletRequest req, HttpServletResponse resp,
			HealthConfigurations configurations) {
		health = new ArrayList<>();

		for (HealthConfiguration config : configurations.getConfigList()) {

			String type = config.getType();
			String visibility = config.getVisibility();
			HandlerMap configType = HandlerMap.valueOf(type);

			if (visibility.equals("ON")) {
				Handler handler = configType.gethandler();
				HealthInfo healthInfo = handler.getInfo(req, resp, config);
				health.add(healthInfo);

			}

		}

		return health;

	}

}
