package app.health.manager;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import app.health.checker.HealthChecker;
import app.health.config.HealthConfigurations;
import app.health.config.loader.LazyConfigLoader;
import app.health.marshaller.JsonMarshaller;
import app.health.marshaller.ResponseMarshallar;

public class HealthManager {

	private String response;

	public String getResponse(HttpServletRequest req, HttpServletResponse resp, AtomicBoolean changed) {

		// Only loads on first request or if Config is loaded
		LazyConfigLoader lazyConfigLoader = LazyConfigLoader.loadConfiguration(changed);

		HealthConfigurations healthConfigurations = lazyConfigLoader.getConfiguration();
		HealthChecker healthChecker = new HealthChecker();
		ResponseMarshallar marshallar = new JsonMarshaller();
		response = marshallar.marshall(healthChecker.checkHealth(req, resp, healthConfigurations));
		return response;

	}

}
