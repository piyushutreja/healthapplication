package app.health.entities;

public class DataBaseInfo implements HealthInfo {

	private String id;
	private String schema;
	private String status;

	
	private String server;
	


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	


	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}


}
