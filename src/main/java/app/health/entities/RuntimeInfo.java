package app.health.entities;

public class RuntimeInfo implements HealthInfo{

	private String id;
	private String maxMemory;
	private String totalMemory;
	private String freeMemory;
	private String javaVersion;	


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getJavaVersion() {
		return javaVersion;
	}

	public void setJavaVersion(String javaVersion) {
		this.javaVersion = javaVersion;
	}

	public String getMaxMemory() {
		return maxMemory;
	}

	public void setMaxMemory(String maxMemory) {
		this.maxMemory = maxMemory;
	}

	public String getTotalMemory() {
		return totalMemory;
	}

	public void setTotalMemory(String totalMemory) {
		this.totalMemory = totalMemory;
	}

	public String getFreeMemory() {
		return freeMemory;
	}

	public void setFreeMemory(String freeMemory) {
		this.freeMemory = freeMemory;
	}

}
