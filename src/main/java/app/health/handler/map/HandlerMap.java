package app.health.handler.map;

import app.health.handler.DataBaseHandler;
import app.health.handler.Handler;
import app.health.handler.JndiDatasourceHandler;
import app.health.handler.RuntimeHandler;
import app.health.handler.ServerInfoHandler;
import app.health.handler.SpringSessionFactoryHandler;

public enum HandlerMap {
	
	springSessionFactory(new SpringSessionFactoryHandler()),database(new DataBaseHandler()),server(new ServerInfoHandler()),memory(new RuntimeHandler()),jndidatasource(new JndiDatasourceHandler());


	private Handler handler;

	public Handler gethandler() {
		return handler;
	}

	HandlerMap(Handler handler) {
		this.handler = handler;
	}
}
