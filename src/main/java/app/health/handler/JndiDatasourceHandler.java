package app.health.handler;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import app.health.config.HealthConfiguration;
import app.health.config.HealthProperty;
import app.health.entities.DataBaseInfo;
import app.health.entities.HealthInfo;

public class JndiDatasourceHandler implements Handler {

	String jndi;
	private DataBaseInfo dataBaseInfo;
	Connection conn = null;
	DataSource ds = null;
	Statement statement = null;
	ResultSet result = null;
	DatabaseMetaData metadata;
	String server;

	@SuppressWarnings("unused")
	private void checkDatabase() throws Exception {

		InitialContext cxt = new InitialContext();

		if (cxt == null) {
			throw new Exception("Issue in JNDI ocntext");
		}

		ds = (DataSource) cxt.lookup(jndi);
		conn = ds.getConnection();

		if (ds == null) {
			throw new Exception("Data source not found!");
		}
		if (conn == null) {
			throw new Exception("Connection issue");
		}

		statement = conn.createStatement();

		result = statement.executeQuery("SELECT @@hostname");

		while (result.next()) {
			server = result.getString(1);

		}

		metadata = conn.getMetaData();

		dataBaseInfo.setStatus("Databse Up");
		dataBaseInfo.setSchema(conn.getCatalog());

		dataBaseInfo.setServer(server);

	}

	@Override
	public HealthInfo getInfo(HttpServletRequest req, HttpServletResponse resp, HealthConfiguration healthconfig) {

		List<HealthProperty> propertyList = healthconfig.getPropertyList();

		for (HealthProperty healthProperty : propertyList) {

			if (healthProperty.getName().equals("jndi")) {
				jndi = healthProperty.getValue();
			}

		}

		dataBaseInfo = new DataBaseInfo();
		dataBaseInfo.setId(healthconfig.getId());
		dataBaseInfo.setStatus("Databse Down");

		try {
			checkDatabase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return dataBaseInfo;

	}

}
