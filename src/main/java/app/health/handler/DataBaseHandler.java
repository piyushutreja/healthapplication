package app.health.handler;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import app.health.config.HealthConfiguration;
import app.health.config.HealthProperty;
import app.health.entities.DataBaseInfo;
import app.health.entities.HealthInfo;;

public class DataBaseHandler implements Handler {

	private Connection conn;
	private DataBaseInfo dataBaseInfo;

	private String DB_URL;
	private String USER;
	private String PASS;
	Statement statement = null;
	ResultSet result = null;
	DatabaseMetaData metadata;
	private String server;

	private void checkDatabase() throws Exception {

		Class.forName("com.mysql.jdbc.Driver");

		conn = DriverManager.getConnection(DB_URL, USER, PASS);

		statement = conn.createStatement();

		result = statement.executeQuery("SELECT @@hostname");
		
		  while (result.next()) {
	             server = result.getString(1);            
	          
	        }
	   


		metadata=   conn.getMetaData();
		dataBaseInfo.setStatus("Databse Up");
		dataBaseInfo.setSchema(conn.getCatalog());
		dataBaseInfo.setServer(server);

		

	}

	@Override
	public HealthInfo getInfo(HttpServletRequest req, HttpServletResponse resp, HealthConfiguration healthconfig) {

		List<HealthProperty> propertyList = healthconfig.getPropertyList();

		for (HealthProperty healthProperty : propertyList) {

			switch (healthProperty.getName()) {

			case "URL":
				DB_URL = healthProperty.getValue();
				break;

			case "username":
				USER = healthProperty.getValue();
				break;

			case "password":
				PASS = healthProperty.getValue();
				break;

			}

		}

		dataBaseInfo = new DataBaseInfo();
		dataBaseInfo.setId(healthconfig.getId());
		dataBaseInfo.setStatus("Databse Down");

		try {
			checkDatabase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return dataBaseInfo;

	}

}
