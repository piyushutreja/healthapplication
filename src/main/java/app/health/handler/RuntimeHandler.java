package app.health.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.health.config.HealthConfiguration;
import app.health.entities.HealthInfo;
import app.health.entities.RuntimeInfo;

public class RuntimeHandler implements Handler {

	public RuntimeInfo getInfo() {
		RuntimeInfo runtimeInfo = new RuntimeInfo();
		String maxMemory =toStringMB(Runtime.getRuntime().maxMemory());
		String totalMemory = toStringMB(Runtime.getRuntime().totalMemory());
		String freeMemory = toStringMB(Runtime.getRuntime().freeMemory());
		runtimeInfo.setMaxMemory(maxMemory);
		runtimeInfo.setTotalMemory(totalMemory);
		runtimeInfo.setFreeMemory(freeMemory);
		runtimeInfo.setJavaVersion(System.getProperty("java.version"));

		return runtimeInfo;

	}
	
	private String toStringMB(long data)
	{
		Long longData = new Long(data/1048576);
		return longData.toString();
	}

	@Override
	public HealthInfo getInfo(HttpServletRequest req, HttpServletResponse resp, HealthConfiguration healthconfig) {
		RuntimeInfo runtimeInfo = new RuntimeInfo();
		String maxMemory =toStringMB(Runtime.getRuntime().maxMemory());
		String totalMemory = toStringMB(Runtime.getRuntime().totalMemory());
		String freeMemory = toStringMB(Runtime.getRuntime().freeMemory());
		runtimeInfo.setId(healthconfig.getId());
		runtimeInfo.setMaxMemory(maxMemory);
		runtimeInfo.setTotalMemory(totalMemory);
		runtimeInfo.setFreeMemory(freeMemory);
		runtimeInfo.setJavaVersion(System.getProperty("java.version"));

		return runtimeInfo;
	}

}
