package app.health.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.health.config.HealthConfiguration;
import app.health.entities.HealthInfo;
import app.health.entities.ServerInfo;

public class ServerInfoHandler implements Handler {





	@Override
	public HealthInfo getInfo(HttpServletRequest req, HttpServletResponse resp, HealthConfiguration healthconfig) {
		ServerInfo serverInfo = new ServerInfo();
		String serverName = req.getSession(true).getServletContext().getServerInfo();	
		int serverPort = req.getServerPort();
		serverInfo.setId(healthconfig.getId());
		serverInfo.setServerName(serverName);
		serverInfo.setServerPort(serverPort);

		return serverInfo;
	}

}
