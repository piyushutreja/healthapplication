package app.health.handler;

import java.sql.DatabaseMetaData;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import app.health.config.HealthConfiguration;
import app.health.config.spring.SpringConfiguration;
import app.health.entities.DataBaseInfo;
import app.health.entities.HealthInfo;


public class SpringSessionFactoryHandler implements Handler {

	private ApplicationContext applicationContext;

	private DataBaseInfo dataBaseInfo;


	public void checkDatabase() throws Exception{

		SessionFactory sessionFactory = (SessionFactory) applicationContext.getBean("sessionFactory");
		Session session = sessionFactory.openSession();

		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			String sql = "SELECT @@hostname,DATABASE()";

			SQLQuery query = session.createSQLQuery(sql);
			@SuppressWarnings("unchecked")
			List<Object[]> rows = query.list();

			for (Object[] row : rows) {

				dataBaseInfo.setServer(row[0].toString());
				dataBaseInfo.setSchema(row[1].toString());

			}

			tx.commit();

		} finally {
			session.close();
		}

		dataBaseInfo.setStatus("Databse Up");
		
	}

	public HealthInfo getInfo(HttpServletRequest req, HttpServletResponse resp, HealthConfiguration healthconfig) {
		applicationContext = SpringConfiguration.getApplicationContext();	
		dataBaseInfo = new DataBaseInfo();
		dataBaseInfo.setId(healthconfig.getId());
		dataBaseInfo.setStatus("Databse Down");
		
		
		try {
			checkDatabase();
		} catch (Exception e) {
		
			e.printStackTrace();
		}

		return dataBaseInfo;

	}

}
