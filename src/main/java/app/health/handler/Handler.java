package app.health.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.health.config.HealthConfiguration;
import app.health.entities.HealthInfo;


public interface Handler {

	public HealthInfo getInfo(HttpServletRequest req, HttpServletResponse resp,HealthConfiguration healthconfig);

}
